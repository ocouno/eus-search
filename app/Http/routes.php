<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('sanctions/import', 'Entities\EntitiesController@import');
Route::get('sanctions/importEntities', 'Entities\EntitiesController@importEntities');
Route::get('sanctions/importNames', 'Entities\EntitiesController@importNames');
Route::get('sanctions/importAddresses', 'Entities\EntitiesController@importAddresses');
Route::get('sanctions/importBirths', 'Entities\EntitiesController@importBirths');
Route::get('sanctions/importPassports', 'Entities\EntitiesController@importPassports');
Route::get('sanctions/importCitizens', 'Entities\EntitiesController@importCitizens');
Route::get('sanctions/search', 'Entities\EntitiesController@search');
Route::get('sanctions/entity/{id}', 'Entities\EntitiesController@showEntity');