<?php namespace App\Http\Controllers\Entities;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Pagination\LengthAwarePaginator;
use DB;

class EntitiesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{ 
	}

	public function import()
	{
		$filename = 'global.xml';
		// Check if file is older than 24 hours
		if ( (time()-filectime($filename)) > 86400 ) {
			$source = 'http://ec.europa.eu/external_relations/cfsp/sanctions/list/version4/global/global.xml';
			$current = file_get_contents($source);
			file_put_contents($filename, $current);

			// If the file exists deleve all DB entries and start the import process
			if (file_exists($filename)) {
				DB::table('entities')->delete();
				DB::table('names')->delete();
				DB::table('addresses')->delete();
				DB::table('births')->delete();
				DB::table('passports')->delete();
				DB::table('citizens')->delete();
				return redirect('sanctions/importEntities');
			}
		} else {
			$message = "This import is up to date";
			return $message;
		}
	}

	// Import script split with redirects for performance

	public function importEntities() 
	{
		$xml = simplexml_load_file('global.xml'); 
		foreach ($xml->ENTITY as $entities) {
			DB::table('entities')->insert(
			    [
					'id' => $entities->attributes()->Id,
					'type' => $entities->attributes()->Type,
					'legal_basis' => $entities->attributes()->legal_basis,
					'pdf_link' => $entities->attributes()->pdf_link,
					'reg_date' => $entities->attributes()->reg_date,
					'programme' => $entities->attributes()->programme,
					'remark' => $entities->attributes()->remark
			    ]
			);
		}
		return redirect('sanctions/importNames');
	}

	public function importNames() 
	{
		$xml = simplexml_load_file('global.xml'); 
		foreach ($xml->ENTITY as $entities) {
			foreach ($entities->NAME as $name) {
				DB::table('names')->insert(
				    [
						'entity_id' => $name->attributes()->Entity_id,
						'legal_basis' => $name->attributes()->legal_basis,
						'pdf_link' => $name->attributes()->pdf_link,
						'programme' => $name->attributes()->programme,
						'reg_date' => $name->attributes()->reg_date,
						'lastname' => $name->LASTNAME,
						'firstname' => $name->FIRSTNAME,
						'middlename' => $name->MIDDLENAME,
						'wholename' => $name->WHOLENAME,
						'gender' => $name->GENDER,
						'title' => $name->TITLE,
						'function' => $name->FUNCTION,
						'language' => $name->LANGUAGE
				    ]
				);
			}
		}
		return redirect('sanctions/importAddresses');
	}

	public function importAddresses() {
		$xml = simplexml_load_file('global.xml'); 
		foreach ($xml->ENTITY as $entities) {
			foreach ($entities->ADDRESS as $address) {
				DB::table('addresses')->insert(
				    [
						'entity_id' => $address->attributes()->Entity_id,
						'legal_basis' => $address->attributes()->legal_basis,
						'pdf_link' => $address->attributes()->pdf_link,
						'programme' => $address->attributes()->programme,
						'reg_date' => $address->attributes()->reg_date,
						'number' => $address->NUMBER,
						'street' => $address->STREET,
						'zipcode' => $address->ZIPCODE,
						'city' => $address->CITY,
						'country' => $address->COUNTRY,
						'other' => $address->OTHER
				    ]
				);
			}
		}
		return redirect('sanctions/importBirths');
	}

	public function importBirths() 
	{
		$xml = simplexml_load_file('global.xml'); 
		foreach ($xml->ENTITY as $entities) {
			foreach ($entities->BIRTH as $births) {
				DB::table('births')->insert(
				    [
						'entity_id' => $births->attributes()->Entity_id,
						'legal_basis' => $births->attributes()->legal_basis,
						'pdf_link' => $births->attributes()->pdf_link,
						'programme' => $births->attributes()->programme,
						'reg_date' => $births->attributes()->reg_date,
						'date' => $births->DATE,
						'place' => $births->PLACE,
						'country' => $births->COUNTRY
				    ]
				);
			}
		}
		return redirect('sanctions/importPassports');
	}

	public function importPassports() 
	{
		$xml = simplexml_load_file('global.xml'); 
		foreach ($xml->ENTITY as $entities) {
			foreach ($entities->PASSPORT as $passport) {
				DB::table('passports')->insert(
				    [
						'entity_id' => $passport->attributes()->Entity_id,
						'legal_basis' => $passport->attributes()->legal_basis,
						'pdf_link' => $passport->attributes()->pdf_link,
						'programme' => $passport->attributes()->programme,
						'reg_date' => $passport->attributes()->reg_date,
						'number' => $passport->NUMBER,
						'country' => $passport->COUNTRY
				    ]
				);
			}
		}
		return redirect('sanctions/importCitizens');
	}

	public function importCitizens() {
		$xml = simplexml_load_file('global.xml'); 
		foreach ($xml->ENTITY as $entities) {
			foreach ($entities->CITIZEN as $citizen) {
				DB::table('citizens')->insert(
				    [
						'entity_id' => $citizen->attributes()->Entity_id,
						'legal_basis' => $citizen->attributes()->legal_basis,
						'pdf_link' => $citizen->attributes()->pdf_link,
						'programme' => $citizen->attributes()->programme,
						'reg_date' => $citizen->attributes()->reg_date,
						'country' => $citizen->COUNTRY
				    ]
				);
			}
		}
		return redirect('sanctions/createIndex');
	}

	public function createIndex() {
		DB::delete('delete from dataindex');
		DB::raw('INSERT INTO dataindex (
			type,
			fullname,
			entity_id,
			entity_regdate,
			name_id,
			name_regdate,
			addresses_id,
			addresses_regdate,
			citizens_id,
			citizens_regdate,
			births_id,
			births_regdate,
			programme
		) SELECT
			entities.type,
			`names`.wholename,
			`entities`.id,
			`entities`.reg_date,
			`names`.id,
			`names`.reg_date,
			`addresses`.id,
			`addresses`.reg_date,
			`citizens`.id,
			`citizens`.reg_date,
			`births`.id,
			`births`.reg_date,
			`names`.programme
		FROM
			`names`
		LEFT OUTER JOIN entities ON `names`.entity_id = entities.id
		LEFT OUTER JOIN addresses ON addresses.entity_id = entities.id
		LEFT OUTER JOIN citizens ON citizens.entity_id = entities.id
		LEFT OUTER JOIN births ON births.entity_id = entities.id');
		return redirect('search');
	}

	public function Search() 
	{

		$request = Request::all();
		$request['et'] = ( isset($request['et'] ) ) ? $request['et'] : null ;
		$request['q'] = ( isset($request['q'] ) ) ? $request['q'] : null ;
		$request['r'] = ( isset($request['r'] ) ) ? $request['r'] : null ;


		$orders = array( 'entity_regdate', 'name_regdate', 'entity_regdate', 'addresses_regdate', 'births_regdate', 'citizens_regdate' );
		$order = ( isset($request['order']) && in_array( $request['order'], $orders ) ) ? $request['order'] : 'name_regdate' ;

		$regions = DB::table('dataindex')->distinct()->select('programme')->orderby('programme','ASC')->get();


		$sanctions = DB::table('dataindex')
            ->select('dataindex.entity_id','dataindex.type','dataindex.fullname','dataindex.entity_regdate','dataindex.name_regdate','dataindex.addresses_regdate','dataindex.citizens_regdate','dataindex.births_regdate','dataindex.programme')
            ->where(function($query) use ($request) {
				if( isset($request['q']) && !empty($request['q'] ) ){
					$query->where('dataindex.fullname','LIKE','%'.$request['q'].'%');
				}
		        if ( isset($request['et']) && !empty($request['et'] ) ) {
					$query->where('dataindex.type',$request['et']);
			    }
		        if ( isset($request['r']) && !empty($request['r'] ) ) {
					$query->where('dataindex.programme',$request['r']);
			    }
            })
            ->orderby($order,'DESC')
            ->paginate(20);

		return view('search')->with( array('sanctions' => $sanctions,'regions' => $regions, 'et' => $request['et'], 'order' => $order, 'q' => $request['q'], 'r' => $request['r'] ) );
	}


	public function showEntity($id) {


		$entities = DB::table('entities')->where('id',$id)->get();
		$names = DB::table('names')->where('entity_id',$id)->orderby('reg_date','DESC')->get();
		$addresses = DB::table('addresses')->where('entity_id',$id)->orderby('reg_date','DESC')->get();
		$passports = DB::table('passports')->where('entity_id',$id)->orderby('reg_date','DESC')->get();
		$citizens = DB::table('citizens')->where('entity_id',$id)->orderby('reg_date','DESC')->get();
		$births = DB::table('births')->where('entity_id',$id)->orderby('reg_date','DESC')->get();

		return view('entities')->with( array(
			'entity' => $entities[0],
			'names' => $names,
			'addresses' => $addresses,
			'passports' => $passports,
			'citizens' => $citizens,
			'births' => $births,
		 ) );
	}

}

