<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Sanctions Search</title>

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<div class="row">
		<div class="col-md-12">


			<form class="form-inline" method="get">
				<div class="form-group">
					<ul class="nav nav-pills">
						<li role="presentation" @if ( $et == '' ) class="active" @endif><a href="/sanctions/search/">All</a></li>
						<li role="presentation" @if ( $et == 'p' ) class="active" @endif><a href="?et=p&q={{ $q }}&r={{ $r }}">Personal entities</a></li>
						<li role="presentation" @if ( $et == 'e' ) class="active" @endif><a href="?et=e&q={{ $q }}&r={{ $r }}">Entity</a></li>
					</ul>
				</div>
				<div class="form-group">
					<input type="text" name="q" class="form-control" id="exampleInputName2" placeholder="Search Full name..." value="{{ $q }}">
					<button type="submit" class="btn btn-default">Search</button>
				</div>

				<div class="form-group">
					<div class="dropdown">
						<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							View by region
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
							@foreach ($regions as $region)
							<li><a href="?et={{ $et }}&q={{ $q }}&r={{ $region->programme }}">{{ $region->programme }}</a></li>
							@endforeach
						</ul>
					</div>
				</div>
			</form>

			<div class="table-responsive">

				
				<div class="pagination">{!! $sanctions->appends( ['et' => $et,'order' => $order,'q' => $q,'r' => $r] )->render() !!}</div>

				<table class="table table-striped">
					<thead>
						<tr>
							<th>type</th>
							<th>Full name</th>
							<th>Region</th>
							<th><a href="?order=entity_regdate&et={{ $et }}&q={{ $q }}&r={{ $r }}">Entity date</a></th>
							<th><a href="?order=name_regdate&et={{ $et }}&q={{ $q }}&r={{ $r }}">Name date</a></th>
							<th><a href="?order=addresses_regdate&et={{ $et }}&q={{ $q }}&r={{ $r }}">Address date</a></th>
							<th><a href="?order=citizens_regdate&et={{ $et }}&q={{ $q }}&r={{ $r }}">Citizens date</a></th>
							<th><a href="?order=births_regdate&et={{ $et }}&q={{ $q }}&r={{ $r }}">Births date</a></th>
						</tr>
					</thead>
					<tbody>
						@foreach ($sanctions as $sanction)
						<tr>
							<td>{{ $sanction->type }}</td>
							<td><a href="/sanctions/entity/{{ $sanction->entity_id }}/">{{ $sanction->fullname }}</td>
							<td>{{ $sanction->programme }}</td>
							<td>{{ $sanction->entity_regdate }}</td>
							<td>{{ $sanction->name_regdate }}</td>
							<td>{{ $sanction->addresses_regdate }}</td>
							<td>{{ $sanction->citizens_regdate }}</td>
							<td>{{ $sanction->births_regdate }}</td>
						</tr>
						@endforeach
					</tbody>
				</table>

				<div class="pagination">{!! $sanctions->appends( ['et' => $et,'order' => $order,'q' => $q,'r' => $r] )->render() !!}</div>

				

			</div>
		</div>
	</div>

	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
</body>
</html>
