<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Sanctions Search</title>

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<div class="row">
		<div class="col-md-12">
			<h1>Entity {{ $entity->id }}</h1>
			<table class="table table-bordered">
				<tr>
					<th>legal basis</th>
					<th>type</th>
					<th>programme</th>
					<th>Reg date</th>
					<th>PDF</th>
				</tr>
				<tr>
					<td>{{ $entity->legal_basis }}</td>
					<td>{{ $entity->type }}</td>
					<td>{{ $entity->programme }}</td>
					<td>{{ $entity->reg_date }}</td>
					<td><a href="{{ $entity->pdf_link }}">Download</a></td>
				</tr>
				@if (strlen($entity->remark) > 0)
				<tr>
					<td colspan="5">Remark: {{ $entity->remark }}</td>
				</tr>
				@endif
			</table>

			@if( count($names) > 0 )
			<h1>Names</h1>
			<table class="table table-bordered">
				<tr>
					<th>Reg date</th>
					@if ( $entity->type == 'P' )
					<th>Title</th>
					<th>Firstname</th>
					<th>Mid</th>
					<th>Lastname</th>
					<th>Gender</th>
					@endif
					<th>Wholename</th>
					<th>Programme</th>
					<th>Function</th>
					<th>Legal basis</th>
					<th>Download</th>
				</tr>
				@foreach( $names as $name)
				<tr>
					<td>{{ $name->reg_date }}</td>
					@if ( $entity->type == 'P' )
					<td>{{ $name->title }}</td>
					<td>{{ $name->firstname }}</td>
					<td>{{ $name->middlename }}</td>
					<td>{{ $name->lastname }}</td>
					<td>{{ $name->gender }}</td>
					@endif
					<td>{{ $name->wholename }}</td>
					<td>{{ $name->programme }}</td>
					<td>{{ $name->legal_basis }}</td>
					<td><a href="{{ $name->pdf_link }}">Download</a></td>
				</tr>
				@if (strlen($name->function) > 0)
				<tr>
					<td colspan="10">Function: {{ $name->function }}</td>
				</tr>
				@endif
				@endforeach
			</table>
			@endif

			@if( count($addresses) > 0 )
			<h1>Addresses</h1>
			<table class="table table-bordered">
				<tr>
					<th>Number</th>
					<th>Street</th>
					<th>Zipcode</th>
					<th>City</th>
					<th>Country</th>
					<th>Other</th>
					<th>Reg date</th>
					<th>Legal Basis</th>
					<th>Programme</th>
					<th>Download</th>
				</tr>
				@foreach( $addresses as $address )
				<tr>
					<td>{{ $address->number }}</td>
					<td>{{ $address->street }}</td>
					<td>{{ $address->zipcode }}</td>
					<td>{{ $address->city }}</td>
					<td>{{ $address->country }}</td>
					<td>{{ $address->other }}</td>
					<td>{{ $address->reg_date }}</td>
					<td>{{ $address->legal_basis }}</td>
					<td>{{ $address->programme }}</td>
					<td><a href="{{ $address->pdf_link }}">Download</a></td>
				</tr>
				@endforeach
			</table>
			@endif

			@if( count($passports) > 0 )
			<h1>Passports</h1>
			<table class="table table-bordered">
				<tr>
					<th>number</th>
					<th>country</th>
					<th>Reg date</th>
					<th>Legal Basis</th>
					<th>Programme</th>
					<th>Download</th>
				</tr>
				@foreach( $passports as $passport )
				<tr>
					<td>{{ $passport->number }}</td>
					<td>{{ $passport->country }}</td>
					<td>{{ $passport->reg_date }}</td>
					<td>{{ $passport->legal_basis }}</td>
					<td>{{ $passport->programme }}</td>
					<td><a href="{{ $passport->pdf_link }}">Download</a></td>
				</tr>
				@endforeach
			</table>
			@endif

			@if( count($citizens) > 0 )
			<h1>Citizens</h1>
			<table class="table table-bordered">
				<tr>
					<th>country</th>
					<th>Reg date</th>
					<th>Legal Basis</th>
					<th>Programme</th>
					<th>Download</th>
				</tr>
				@foreach( $citizens as $citizen )
				<tr>
					<td>{{ $citizen->country }}</td>
					<td>{{ $citizen->reg_date }}</td>
					<td>{{ $citizen->legal_basis }}</td>
					<td>{{ $citizen->programme }}</td>
					<td><a href="{{ $citizen->pdf_link }}">Download</a></td>
				</tr>
				@endforeach
			</table>
			@endif


			@if( count($births) > 0 )
			<h1>Births</h1>
			<table class="table table-bordered">
				<tr>
					<th>Date</th>
					<th>Place</th>
					<th>Country</th>
					<th>Reg date</th>
					<th>Legal Basis</th>
					<th>Programme</th>
					<th>Download</th>
				</tr>
				@foreach( $births as $birth )
				<tr>
					<td>{{ $birth->date }}</td>
					<td>{{ $birth->place }}</td>
					<td>{{ $birth->country }}</td>
					<td>{{ $birth->reg_date }}</td>
					<td>{{ $birth->legal_basis }}</td>
					<td>{{ $birth->programme }}</td>
					<td><a href="{{ $birth->pdf_link }}">Download</a></td>
				</tr>
				@endforeach
			</table>
			@endif

		</div>
	</div>
	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
</body>
</html>
