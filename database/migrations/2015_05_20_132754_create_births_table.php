<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBirthsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('births', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
			$table->integer('entity_id');
			$table->string('legal_basis');
			$table->string('pdf_link');
			$table->string('programme');
			$table->date('reg_date');
			$table->string('date');
			$table->string('place');
			$table->string('country');



		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('births');
	}

}










