<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('addresses', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
			$table->integer('entity_id');
			$table->string('legal_basis');
			$table->string('pdf_link');
			$table->string('programme');
			$table->date('reg_date');
			$table->string('number');
			$table->string('street');
			$table->string('zipcode');
			$table->string('city');
			$table->string('country');
			$table->string('other');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('addresses');
	}

}
