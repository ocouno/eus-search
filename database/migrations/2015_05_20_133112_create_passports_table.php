<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePassportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('passports', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
			$table->integer('entity_id');
			$table->string('legal_basis');
			$table->date('reg_date');
			$table->string('pdf_link');
			$table->string('programme');
			$table->string('number');
			$table->string('country');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('passports');
	}

}
