<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitizensTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('citizens', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
			$table->string('country');
			$table->integer('entity_id');
			$table->string('legal_basis');
			$table->string('pdf_link');
			$table->string('programme');
			$table->date('reg_date');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('citizens');
	}

}