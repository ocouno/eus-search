<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNamesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('names', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
			$table->integer('entity_id');
			$table->string('legal_basis');
			$table->string('pdf_link');
			$table->string('programme');
			$table->date('reg_date');
			$table->string('lastname');
			$table->string('firstname');
			$table->string('middlename');
			$table->string('wholename');
			$table->string('gender');
			$table->string('title');
			$table->string('function');
			$table->string('language');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('names');
	}

}
