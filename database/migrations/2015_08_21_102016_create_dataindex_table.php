<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataindexTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dataindex', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('type');
			$table->string('fullname');
			$table->integer('entity_id');
			$table->date('entity_regdate');
			$table->integer('name_id');
			$table->date('name_regdate');
			$table->integer('addresses_id');
			$table->date('addresses_regdate');
			$table->integer('citizens_id');
			$table->date('citizens_regdate');
			$table->integer('births_id');
			$table->date('births_regdate');
			$table->string('programme');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('dataindex');
	}

}
